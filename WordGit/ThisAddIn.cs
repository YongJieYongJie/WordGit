﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace WordGit
{
    public partial class ThisAddIn
    {
        public const string pathToCmd = @"C:\WINDOWS\system32\cmd.exe";
        public const string pathToGit = @"D:\Program Files\Git\cmd\git.exe";
        public const string pathToTortoiseGitScript = @"D:\\Program Files\\TortoiseGit\\Diff-Scripts\\diff-doc.js";
        public const string pathToPandoc = @"D:\Program Files (x86)\Pandoc\pandoc";

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.DocumentBeforeSave +=
                new Word.ApplicationEvents4_DocumentBeforeSaveEventHandler(Application_DocumentBeforeSave);
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new WordGitRibbon();
        }

        void Application_DocumentBeforeSave(Word.Document Doc, ref bool SaveAsUI, ref bool Cancel)
        {
        }

        public static void saveAVersion()
        {
            if (isSavingExistingLocalFile())
            {
                if (!isGitRepoInitialized())
                {
                    initializeGitRepo();
                }
                string commitMessage = promptForCommitMessage();
                Globals.ThisAddIn.Application.ActiveDocument.Save();
                convertToMarkdown();
                stageAndCommitChanges(commitMessage);
            }
            else
            {
                // TODO inform user to save again to initialize GIT repository
            }
        }

        public static void diffLastCommit()
        {
            gitDifftool("HEAD~1");
        }

        public static void push()
        {
            if (isRemoteRepoConfigured())
            {
                gitPush("");
            }
            else
            {
                // not prompting for username/password because using Git Credentials Manager for Windows
                string remoteUrl = Prompt.ShowDialog("Please enter the server URL:", "Save to central server");
                runGitCommand("remote add origin " + remoteUrl);
                runGitCommand("push --set-upstream " + remoteUrl + " master");
            }
        }

        public static void changeUserAndEmail()
        {
            if (!isGitRepoInitialized())
            {
                initializeGitRepo();
            }
            string user = Prompt.ShowDialog("Please enter username:", "Change User");
            string email = Prompt.ShowDialog("Please enter email:", "Change User");
            runGitCommand("config --local user.name \"" + user + "\"");
            runGitCommand("config --local user.email \"" + email + "\"");
        }

        private static bool isRemoteRepoConfigured()
        {
            string gitRemote = runGitCommand("remote");
            return !String.IsNullOrEmpty(gitRemote);
        }

        public static void clone()
        {
            string remoteUrl = Prompt.ShowDialog("Please enter the document URL:", "Download new document");

            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "Please select the root folder to save the new document\n(a new folder will be created)";

            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                gitClone(remoteUrl, dialog.SelectedPath);
            }
        }

        public static void pull()
        {
            runGitCommand("fetch");

            if (hasUncommittedChanges())
            {
                stageAndCommitChanges("Automatic commit for updating");
            }

            runGitCommand("checkout .");
            if (canFastForward())
            {
                string workingdir = Globals.ThisAddIn.Application.ActiveDocument.Path;
                string filename = Globals.ThisAddIn.Application.ActiveDocument.Name;
                // saved as a temporary document so as to close file handle that will otherwise prevent git reset;
                // temporary document kept open to retain easy access to document path in runGitCommand() via
                // Globals.ThisAddIn.Application.ActiveDocument.Path, need to come up with better design
                Globals.ThisAddIn.Application.ActiveDocument.SaveAs2(Globals.ThisAddIn.Application.ActiveDocument.Path + "\\TEMP.docx");

                string gitPullFfOnly = runGitCommand("pull --ff-only");
                Globals.ThisAddIn.Application.ActiveDocument.Close();
                Globals.ThisAddIn.Application.Documents.Open(workingdir + "\\" + filename);

                string message = "Successfully updated from central server";
                string caption = "Update Success!";
                MessageBoxButtons buttons = MessageBoxButtons.OK;

                MessageBox.Show(message, caption, buttons, MessageBoxIcon.Information);
            }
            else
            {
                string message = "There are conflicting changes between the central server and local copy of the document. "
                                 + "You can compare the documents and decide how to combine the changes.\n\n"
                                 + "Would you like to do that now?";
                string caption = "Automatic Update Failure";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Error);

                if (result == DialogResult.Yes)
                {
                    resolveConflict();
                }
            }
        }

        private static bool hasUncommittedChanges()
        {
            string gitStatus = runGitCommand("status \"" + Globals.ThisAddIn.Application.ActiveDocument.Name + "\"");
            return !gitStatus.Contains("nothing to commit, working directory clean");
        }

        private static bool canFastForward()
        {
            string gitStatus = runGitCommand("status");
            return gitStatus.Contains("and can be fast-forwarded.");
        }

        private static void gitPush(string remoteUrl)
        {
            string gitPush = runGitCommand("push " + remoteUrl);
            if (gitPush.Contains("error: failed to push"))
            {
                informPushFailureAndConfirmIfPull();
            }
            else
            {
                deleteConflictResolutionBranch();
                deleteConflictResolutionFile();
            }
        }

        private static void informPushFailureAndConfirmIfPull()
        {
            string message = "The central server has been updated by someone else, you will need to incorporate those "
                             + "changes before you can save to the central server.\n\n"
                             + "Do you want to download those changes now?";
            string caption = "Unable to save to central server";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Error);

            if (result == DialogResult.Yes)
            {
                resolveConflict();
            }
        }

        private static void deleteConflictResolutionBranch()
        {
            runGitCommand("branch -D TEMP_FOR_CONFLICT_RESOLUTION");
        }

        private static void deleteConflictResolutionFile()
        {
            string tempFilePath = Globals.ThisAddIn.Application.ActiveDocument.Path + "/TEMP.docx";
            if (File.Exists(tempFilePath))
            {
                File.Delete(tempFilePath);
            }
        }

        private static void resolveConflict()
        {
            runGitCommand("fetch");
            runGitCommand("branch TEMP_FOR_CONFLICT_RESOLUTION");

            string workingDir = Globals.ThisAddIn.Application.ActiveDocument.Path;
            string fileName = Globals.ThisAddIn.Application.ActiveDocument.Name;
            // saved as a temporary document so as to close file handle that will otherwise prevent git reset;
            // temporary document kept open to retain easy access to document path in runGitCommand() via
            // Globals.ThisAddIn.Application.ActiveDocument.Path, need to come up with better design
            Globals.ThisAddIn.Application.ActiveDocument.SaveAs2(Globals.ThisAddIn.Application.ActiveDocument.Path + "\\TEMP.docx");
            runGitCommand("reset --hard origin/master");

            gitDifftoolReverse("HEAD..TEMP_FOR_CONFLICT_RESOLUTION", workingDir, fileName);
            // closes the TEMP.docx
            Globals.ThisAddIn.Application.ActiveDocument.Close();
        }

        private static void gitClone(string remoteUrl, string folderPath)
        {
            string gitClone = runGitCommand("clone " + remoteUrl + " \"" + folderPath + "\"");
            openFileInCloneRepo(folderPath);
        }

        private static void openFileInCloneRepo(string repoPath)
        {
            // assumes the repo contains only one .docx file
            string filePath = Directory.GetFiles(repoPath, "*.docx")[0];
            Globals.ThisAddIn.Application.Documents.Open(filePath);
        }

        private static void convertToMarkdown()
        {
            runCmdCommand("\"\"" + pathToPandoc + "\" -f docx -t markdown "
                              + "\"" + Globals.ThisAddIn.Application.ActiveDocument.Name + "\" "
                              + "-o \""+ Globals.ThisAddIn.Application.ActiveDocument.Name + ".md\"\"");
        }

        private static bool isSavingExistingLocalFile()
        {
            bool isSavingNewFile = String.IsNullOrEmpty(Globals.ThisAddIn.Application.ActiveDocument.Path);
            return !isSavingNewFile;
        }

        private static bool isGitRepoInitialized()
        {
            string gitStatus = runGitCommand("status");
            return gitStatus.Contains("On branch") ? true : false;
        }

        private static void initializeGitRepo()
        {
            string gitInit = runGitCommand("init");
            if (!gitInit.Contains("Initialized empty Git repository"))
            {
                throw new System.ApplicationException("Error initializing Git repository");
            }
        }

        private static string promptForCommitMessage()
        {
            return Prompt.ShowDialog("Briefly describe the changes made:", "Save log");
        }

        private static void stageAndCommitChanges(string commitMessage)
        {
            string gitAdd = runGitCommand("add \"" + Globals.ThisAddIn.Application.ActiveDocument.Name + "\" "
                                          + "\"" + Globals.ThisAddIn.Application.ActiveDocument.Name + ".md\"");
            string gitCommit = runGitCommand("commit -m \"" + commitMessage + "\"");
        }

        private static void gitDifftool(string diffRange)
        {
            string workingDir = Globals.ThisAddIn.Application.ActiveDocument.Path;
            string fileName = Globals.ThisAddIn.Application.ActiveDocument.Name;
            gitDifftool(diffRange, workingDir, fileName);
        }

        private static void gitDifftool(string diffRange, string workingDir, string fileName)
        {
            runGitCommand(@"difftool -y -x ""wscript.exe \""" + pathToTortoiseGitScript
                              + @"\"" \""$LOCAL\"" \""" + workingDir + @"/\""\""$REMOTE\"""" " + diffRange
                              + " \"" + fileName + "\"");
        }

        // for when $LOCAL is a relative path and $REMOTE is an absolute path
        private static void gitDifftoolReverse(string diffRange, string workingDir, string fileName)
        {
            runGitCommand(@"difftool -Y -X ""wscript.exe \""" + pathToTortoiseGitScript + @"\"" "
                              + @"\""" + workingDir + @"/\""\""$LOCAL\"" " + @"\""$REMOTE\"""" " + diffRange
                              + " \"" + fileName + "\"");
        }

        private static string runCmdCommand(string commandStr)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;

            process.StartInfo.WorkingDirectory = Globals.ThisAddIn.Application.ActiveDocument.Path;
            process.StartInfo.FileName = pathToCmd;
            process.StartInfo.Arguments = "/C " + commandStr;

            // redirect STDOUT and STDERR
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            string stdout = process.StandardOutput.ReadToEnd();
            string stderr = process.StandardError.ReadToEnd();
            process.WaitForExit();

            return String.IsNullOrEmpty(stdout) ? stderr : stdout;
        }

        private static string runGitCommand(string commandStr)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;

            process.StartInfo.WorkingDirectory = Globals.ThisAddIn.Application.ActiveDocument.Path;
            process.StartInfo.FileName = pathToGit;
            process.StartInfo.Arguments = commandStr;

            // redirect STDOUT and STDERR
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            string stdout = process.StandardOutput.ReadToEnd();
            string stderr = process.StandardError.ReadToEnd();
            process.WaitForExit();

            return String.IsNullOrEmpty(stdout) ? stderr : stdout;
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}

public static class Prompt
{
    public static string ShowDialog(string message, string title)
    {
        Form prompt = new Form()
        {
            Width = 440,
            Height = 160,
            Text = title,
            FormBorderStyle = FormBorderStyle.FixedDialog,
            StartPosition = FormStartPosition.CenterScreen
        };

        Label promptLabel = new Label() { Top = 15, Left = 15, Height = 20, Width = 400, Text = message};
        prompt.Controls.Add(promptLabel);

        TextBox inputTextBox = new TextBox() { Top = 35, Left = 15, Width = 385 };
        prompt.Controls.Add(inputTextBox);

        Button okButton = new Button() { Top = 65, Left = 400 - 100, Width = 100, Text = "OK", DialogResult = DialogResult.OK };
        okButton.Click += (sender, e) => { prompt.Close(); };
        prompt.Controls.Add(okButton);
        prompt.AcceptButton = okButton;

        return prompt.ShowDialog() == DialogResult.OK ? inputTextBox.Text : "";
    }
}
