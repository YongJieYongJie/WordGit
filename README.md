# WordGit

A GIT wrapper for Microsoft Word

### Motivation

To bring the delights of version control that us software engineers
have been enjoying to the masses (AKA users of Micrsoft Word).

### General Design

Using Visual Studio Tools for Office, hooks are added to Microsoft
Word's save action, where users will be prompted as to whether they
want to save a new version in the version control system (i.e.,
GIT). If the user decides so, the document will be saved, committed,
and pushed to the hosted version control system (currently only
GitLab). A Markdown file is also generated (using Pandoc) and
committed along with the document. This is because GIT and version
hosted GIT is not able to show diff of Microsoft Word document, but is
able to show a diff of Markdown files.

User can see the diff on the hosted GIT, or install TortoiseGIT to
view the diff locally. Using TortoiseGIT, users are also able to
revert to earlier versions.

Alternatively, from within Microsoft Word itself, users are able to
select which version to compare against, and the version will be
checked out and compared using Microsoft Word's compare feature.


### Required softwares
Please download and install the following required softwares:
 - Microsoft Word 2013
 - Microsoft Visual Studio
   - Visual Studio 2012 Ultimate, or
   - [Visual Studo 2015 Community][vs_2015_community]
 - Visual Studio Tools for Office [VSTO] Runtime
   - [VSTO 2012][vsto_2012]
   - [VSTO 2015][vsto_2015]
 - [Git for Windows (2.9.0)][git_for_windows]
 - [Git Credential Manager for Windows][git_credential_manager]
   - (this is installed by default together with Git for Windows)
 - [TortoiseGit (2.1.0.0)][tortoise_git]
   - (actually only the diff-ing script is needed...)
 - [Pandoc][pandoc]

[vs_2015_community]: https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx
[vsto_2012]: https://www.microsoft.com/en-us/download/details.aspx?id=48217
[vsto_2015]: https://www.visualstudio.com/en-us/features/office-tools-vs.aspx
[git_for_windows]: https://git-scm.com/download/win
[git_credential_manager]: https://github.com/Microsoft/Git-Credential-Manager-for-Windows
[tortoise_git]: https://tortoisegit.org/download/
[pandoc]: https://pandoc.org/

### Opening/running the solution
1. Run Visual Studio
2. Go to [File] -> [Open] -> [Project/Solution...]
3. Navigate to WordGit root directory and open WordGit.sln
4. Press F5 to build and run WordGit, Microsoft Word should
   automatically start 
5. If there are build solutions, trying creating development key files for
   signing

### Note
WordGit is currently developed on Windows machine only
